---
title: "Module 5: Cryptography for Privacy-Preserving Computation"
type: "Lesson"
permalink: /lessons/lesson05-crypt
excerpt: "Homomorphic encryption enables untrusted parties to perform computation while preserving the privacy of sensitive data [(Lesson site)](https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/)"
collection: lessons
---

Certain encryption methods such as *homomorphic encryption* (HE)
enables untrusted parties to perform computation while preserving the
privacy of sensitive data.
Pervarsive computing---stemming from the widespread use of internet,
mobile computing, and internet-of-things---has led to
the availability of massive amounts of data that can be utilized for
the good of society, such as: smart cities, urban planning,
improving health, education, and many more.
However, there is much concern with personal privacy and/or corporate
confidentiality when sensitive data is transferred and processed in
the clear.
HE can get around this issue by encrypting the data while
allowing limited types of computation in the encrypted form.
HE enables parties such as government and industry to obtain
aggregate or statistical information.
More recently, researchers investigate the use of HE to perform
privacy-preserving machine learning.
In this lesson, we will learn the basics of HE and use it to transfer
"sensitive" data to an untrusted party for computation of statistics.
We will also circle back to parallel computing to speed up HE-based
operations.

> Please check out our Advanced Encryption lesson at the following site:
>
> <https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/>

