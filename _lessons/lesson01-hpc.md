---
title: "Module 1: Introduction to HPC"
#title: "[Module 1: Introduction to HPC](https://deapsecure.gitlab.io/deapsecure-lesson01-hpc/)"
type: "Lesson"
permalink: /lessons/lesson01-hpc
excerpt: "Introduction to high-performance computing on a Linux cluster: UNIX shell interaction, SLURM job scheduler, parallel job launch. [(Lesson site)](https://deapsecure.gitlab.io/deapsecure-lesson01-hpc/)"
#excerpt: "Short description of portfolio item number 1<br/><img src='/images/500x300.png'>"
collection: lessons
---

A high-performance computing (HPC) system, or commonly called a *supercomputer*,
is a set of highly capable computers that are tightly connected with a
high-performance network.
In this lesson module, you will learn about what an HPC system is, how to
access it, how to interact with it, and how to use HPC to complete a
set of computation much sooner than using only personal computers.

After connecting to an HPC system, we will first learn how to use
UNIX command-line interface (shell) and appreciate its power.
This is the *lingua franca* of supercomputing: it is used everywhere
from the interactive terminal sessions, to job submission, and even
data analysis.
We will then learn about a job scheduler (SLURM in particular)
and how to create job scripts that will run on an HPC system.

> Please look at our HPC lesson at the following link:
>
> <https://deapsecure.gitlab.io/deapsecure-lesson01-hpc/>


## Workshop Resources (Workshop Series 2020-2021)
{: id="workshop-resources-ws2020"}

### Presentation Slides

[Presentation slides (Google sheets)][hpc-prez-2020]


### Video Recordings

- [Challenges with Cybersecurity: Why Using HPC & CI Techniques to Address Cybersecurity Challenges](
      https://odumedia.mediaspace.kaltura.com/media/1_79ss9c7y
  )

  Dr. Hongyi Wu discusses the challenges with cyber attacks in the present days.
  He also mentions the benefits of using HPC, big data, machine learning
  to help strengthening cybersecurity.

> (More videos are upcoming!)

{% include links.md %}

[hpc-prez-2020]:             https://docs.google.com/presentation/d/1Ia6VDfR5UzFbK1c9OcNN745KoYDxNby0qTrv0iJZkyg/view

[npc-ws2020-vid01]:          https://odumedia.mediaspace.kaltura.com/media/1_01c33rbz
