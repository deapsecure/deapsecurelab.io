---
title: "Module 6: Parallel and High-Performance Computing"
type: "Lesson"
permalink: /lessons/lesson06-par
excerpt: "Tightly coupled calculations can be efficiently parallelized using industry-standard MPI and OpenMP programming models. [(Lesson site)](https://deapsecure.gitlab.io/deapsecure-lesson06-par/)"
collection: lessons
---

In this lesson, we learn the industry-standard approaches to
parallelize tightly-coupled calculations.
Such calculations are frequently encountered in modeling and simulation
in mathematics, physical sciences, and engineering.
The MPI (Message Passing Interface) library provides capability for
a computation to *scale out* to many, many machines at once.
MPI is available in popular programming languages such as C, C++,
Fortran, Python, Java, and many more.
OpenMP is a programming model that allows one to conveniently
convert a sequential program to a shared-memory parallel program,
and is available in C, C++, and Fortran.
Both MPI and OpenMP are explicitly parallel programming approaches,
where one has to define the data distribution, work sharing, and
the coordination among workers.
MPI and OpenMP can be useful in computationally-intensive simulations
where code performance and efficient interprocess communication are
essential for timely completion of the calculations.

> Please check out our Parallel and High-Performance Computing lesson
> at the following site:
>
> <https://deapsecure.gitlab.io/deapsecure-lesson06-par/>


## Workshop Resources (Spring 2021):
- [Slides](https://docs.google.com/presentation/d/1yPXlCRYdDZtPXttGW2scz8zG5KYtKa6bNmjhJuvhN7g/edit?usp=sharing)

- List of Lessons
    - [Homomorphic Encryption --- A Primer](https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/20-homomorphic-enc/index.html)
    - [Paillier Cryptosystem for Homomorphic Encryption](https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/21-paillier-he/index.html)
    - [Introduction to Parallel Programming](https://deapsecure.gitlab.io/deapsecure-lesson06-par/01-introduction/index.html)
    - [Distributed Memory Programming in Python using mpi4py](https://deapsecure.gitlab.io/deapsecure-lesson06-par/10-distributed-memory-model/index.html)
    - [Parallel Computation with Homomorphic Encryption](https://deapsecure.gitlab.io/deapsecure-lesson06-par/11-mip4py-hands-on-paillier-encryption/index.html)


- Jupyter Notebooks

- [List of videos](https://odumedia.mediaspace.kaltura.com/playlist/details/1_dlnhq3xg)
