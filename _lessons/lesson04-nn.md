---
title: "Module 4: Deep Learning Using Neural Networks"
type: "Lesson"
permalink: /lessons/lesson04-nn
excerpt: "Neural network is a powerful approach to machine learning that can yield extremely high accuracy on complex cognitive tasks. [(Lesson site)](https://deapsecure.gitlab.io/deapsecure-lesson04-nn/)"
collection: lessons
---

In this lesson, we will learn the basics of deep learning based on
artificial neural networks, or simply *neural networks* (NN).
As a specific subset of machine learning (ML),
NN has proven to achieve impressive accuracy
over traditional ML methods.
We will use **TensorFlow** and **Keras** library to build and train NN models
relevant to mobile phone cybersecurity applications.
Many of today's AI-based systems such as speech input processing
(found on your smart phones, Google Home, Alexa, among others),
image recognition, digital assistants, and self-driving cars
are powered by highly complex NN models.

> Please check out our Neural Networks lesson at the following page:
>
> <https://deapsecure.gitlab.io/deapsecure-lesson04-nn/>



## Workshop Resources (Workshop Series 2020-2021)
{: id="workshop-resources-ws2020"}

### Presentation Slides

[Presentation slides (PDF)][nn-prez-2020] by Kazi Aminul Islam



### Jupyter Notebooks

The list of notebooks below reflects the actual sequence of notebooks
taken during ML session of the 2020-2021 workshop series.

*(To download the notebook and the hands-on files, please right-click on the links below and
select "Save Link As..." or a similar menu)*

- [Session 1: Binary Classification with Keras][nn-notebook-ws2020-1] -
  ([html][nn-notebook-ws2020-1-html])
- [Session 2: Classifying Smartphone Apps with Keras][nn-notebook-ws2020-2] -
  ([html][nn-notebook-ws2020-2-html])

(The HTML files were provided for convenient web viewing.)

### Hands-on Files

- [Sherlock hands-on files for ML and NN lessons, except the large files][ML-sherlock]
  ([table of contents][ML-sherlock.contents]) -- This also contains
  the Jupyter notebooks above
- [Sherlock large dataset: `sherlock_2apps`][ML-sherlock-large-2apps]
  ([table of contents][ML-sherlock-large-2apps.contents])
- [Sherlock large dataset: `sherlock_18apps`][ML-sherlock-large-18apps]
  ([table of contents][ML-sherlock-large-18apps.contents])

The hands-on files are packed in ZIP format.
The three ZIP files above are mandatory.
To reconstitute: Unzip all the files, preserving the paths, into the same destination directory.


{% include links.md %}

[nn-prez-2020]:              https://drive.google.com/file/d/1DMj9lM8hgRRw-FOmfNhdfUU5fThWH8zV/view

[nn-notebook-ws2020-1]:      {{ site.deapsecure_nn_lesson }}/files/ws-2020/NN-session-1.ipynb
[nn-notebook-ws2020-2]:      {{ site.deapsecure_nn_lesson }}/files/ws-2020/NN-session-2.ipynb
[nn-notebook-ws2020-1-html]: {{ site.deapsecure_nn_lesson }}/files/ws-2020/NN-session-1.html
[nn-notebook-ws2020-2-html]: {{ site.deapsecure_nn_lesson }}/files/ws-2020/NN-session-2.html

[ML-sherlock]:               {{ site.deapsecure_ml_lesson }}/files/ML-sherlock.zip
[ML-sherlock-large-2apps]:   {{ site.deapsecure_ml_lesson }}/files/ML-sherlock-large-2apps.zip
[ML-sherlock-large-18apps]:  {{ site.deapsecure_ml_lesson }}/files/ML-sherlock-large-18apps.zip

[ML-sherlock.contents]:      {{ site.deapsecure_ml_lesson }}/files/ML-sherlock.contents
[ML-sherlock-large-2apps.contents]: {{ site.deapsecure_ml_lesson }}/files/ML-sherlock-large-2apps.contents
[ML-sherlock-large-18apps.contents]: {{ site.deapsecure_ml_lesson }}/files/ML-sherlock-large-18apps.contents

