---
title: "Module 2: Dealing with Big Data"
#title: "[Module 2: Dealing with Big Data](https://deapsecure.gitlab.io/deapsecure-lesson02-bd/)"
type: "Lesson"
permalink: /lessons/lesson02-bd
excerpt: "Introduction to Pandas, a powerful data processing framework capable of handling large amounts of data in an efficient manner. [(Lesson site)](https://deapsecure.gitlab.io/deapsecure-lesson02-bd/)"
collection: lessons
---

"Big data" is a term that generally describes data whose
characteristics defy "traditional" data processing approaches.
*Having a table of 10 million rows and 500 columns?*
That is too big for a spreadsheet software to handle.
*Having a total of 100 GB worth of log files coming from
over 1000 different computers from a huge data center?*
These would be too long to analyze using system engineer's basic tool
(like UNIX commands chained together) on a single computer.
These are a few examples of "big data" challenges.
In this lesson module, we introduce an efficient way
of handling, processing, and analyzing large amounts of data
using **pandas**.
pandas is the de facto data analysis and manipulation tool
for Python programming language.
The data handling skills introduced in this lesson
form the foundation for the subsequent two lessons
on [machine learning]({% link _lessons/lesson03-ml.md %})
and [neural networks]({% link _lessons/lesson04-nn.md %}).


{% comment %}
<!-- Spark... -->

In this lesson module, we will introduce **Spark**, a general
framework to leverage parallel processing that is well able to ingest
and process massive *volume*, high *velocity*, and/or unwieldly
*variety* of big data.
Under the hood, Spark split up the data analytics task into small
subtasks that can then be executed in parallel.
This allows us to process the amount of data that is impossible to
handle on a single computer, and/or significantly shorten the time
needed to obtain the desired results.
{% endcomment %}

> Please look at our Big Data lesson at the following link:
>
> <https://deapsecure.gitlab.io/deapsecure-lesson02-bd/>


## Workshop Resources (Workshop Series 2020-2021)
{: id="workshop-resources-ws2020"}

### Presentation Slides

[Presentation slides (Google sheets)](https://docs.google.com/presentation/d/1Mo2hWdgHI5ZhnfRYa_kjnFzJ4oI9ybK15OnMKJ6bvzA/edit)


### Jupyter Notebooks

*(To download the notebook and the hands-on files, please right-click on the links below and
select "Save Link As..." or a similar menu)*

- [Session 1: Fundamentals of Pandas][bd-notebook-ws2020-1] -
  ([html][bd-notebook-ws2020-1-html])
- [Session 2: Analytics of Sherlock Data with Pandas][bd-notebook-ws2020-2] -
  ([html][bd-notebook-ws2020-2-html])
- [Session 3: Data Wrangling and Visualization][bd-notebook-ws2020-3] -
  ([html][bd-notebook-ws2020-3-html])

### Hands-on Files

- [Sherlock hands-on files, except the large files][BD-sherlock]
  ([table of contents][BD-sherlock.contents])
- [Sherlock large data file: "sherlock_mystery_2apps.csv"][BD-sherlock-large-2apps]
  ([table of contents][BD-sherlock-large-2apps.contents])
- [Spam-ip based hands-on (legacy, optional)][BD-spam-ip]
  ([table of contents][BD-spam-ip.contents])

The hands-on files are packed in ZIP format.
The first two ZIP files above are mandatory.
To reconstitute: Unzip all the files, preserving the paths, into the same destination directory.


[bd-notebook-ws2020-1]: {{ site.ds_lesson02_root }}/files/ws-2020/BigData-session-1.ipynb
[bd-notebook-ws2020-2]: {{ site.ds_lesson02_root }}/files/ws-2020/BigData-session-2.ipynb
[bd-notebook-ws2020-3]: {{ site.ds_lesson02_root }}/files/ws-2020/BigData-session-3.ipynb
[bd-notebook-ws2020-1-html]: {{ site.ds_lesson02_root }}/files/ws-2020/BigData-session-1.html
[bd-notebook-ws2020-2-html]: {{ site.ds_lesson02_root }}/files/ws-2020/BigData-session-2.html
[bd-notebook-ws2020-3-html]: {{ site.ds_lesson02_root }}/files/ws-2020/BigData-session-3.html
[BD-sherlock]:             {{ site.ds_lesson02_root }}/files/BD-sherlock.zip
[BD-sherlock-large-2apps]: {{ site.ds_lesson02_root }}/files/BD-sherlock-large-2apps.zip
[BD-spam-ip]:              {{ site.ds_lesson02_root }}/files/BD-spam-ip.zip
[BD-sherlock.contents]:    {{ site.ds_lesson02_root }}/files/BD-sherlock.contents
[BD-sherlock-large-2apps.contents]: {{ site.ds_lesson02_root }}/files/BD-sherlock-large-2apps.contents
[BD-spam-ip.contents]:     {{ site.ds_lesson02_root }}/files/BD-spam-ip.contents


### Video Recordings

- ["Pandas: Data Analytics the Python Way” (Overview of Notebook 1)](
      https://odumedia.mediaspace.kaltura.com/media/1_79ss9c7y
  )

>  Key points of the video:
>    - What is Pandas?
>    - Series and DataFrame
>    - Data types

- ["Processing & Analyzing Data with Pandas" (Overview of Notebook 2)](
      https://odumedia.mediaspace.kaltura.com/media/1_i5q0bxw0
  )

>  Key points of the video:
>    - Operation on Data
>    - Filtering 
>    - Sorting
>    - Chaining operations
>    - Grouping & Aggregation
>    - Column operations 
