---
title: "Module 3: Machine Learning"
#title: "[Module 3: Machine Learning](https://deapsecure.gitlab.io/deapsecure-lesson03-ml/)"
type: "Lesson"
permalink: /lessons/lesson03-ml
excerpt: "Machine learning is an approach to program a computer to perform certain intelligent tasks without being explicitly programmed to do so. [(Lesson site)](https://deapsecure.gitlab.io/deapsecure-lesson03-ml/)"
collection: lessons
---

In this lesson, we will learn the basics of machine learning using
Python's powerful machine learning (ML) library called **scikit-learn**.
ML is a branch of artificial intelligence (AI) in
which a computer model is trained to recognize patterns from data
and/or to perform certain functions.
There is a vast number of applications of machine learning in today's
world: ranging from image and face recognition, speech-based input
(found on your smart phones, Google Home, Alexa, among others), smart
thermostats, even all the way to self-driving cars.
Unlike traditional approach, where a program is written line-by-line
to declare all the rules and actions in all possible cases, ML
"learns" from a given input data to discover the pattern found in
the data (often hidden from our limited human ability to recognize them).

> Please check out our Machine Learning lesson at the following site:
>
> <https://deapsecure.gitlab.io/deapsecure-lesson03-ml/>


## Workshop Resources (Workshop Series 2020-2021)
{: id="workshop-resources-ws2020"}

### Presentation Slides

[Presentation slides (Google sheets)][ml-prez-2022] --
last updated Summer 2022


### Jupyter Notebooks

The list of notebooks below reflects the actual sequence of notebooks
taken during ML session of the 2020-2021 workshop series.
The first notebook is actually a notebook from the
[Big Data][deapsecure-bd-lesson]
lesson on *Data Wrangling and Visualization*.

*(To download the notebook and the hands-on files, please right-click on the links below and
select "Save Link As..." or a similar menu)*

- [Session 1: Data Wrangling and Visualization][bd-notebook-ws2020-3] from the Big Data lesson -
  ([html][bd-notebook-ws2020-3-html])
- [Session 2: Data Preprocessing for Machine Learning][ml-notebook-ws2020-2] -
  ([html][ml-notebook-ws2020-2-html])
- [Session 3: Tuning the Machine Learning Model][ml-notebook-ws2020-3] -
  ([html][ml-notebook-ws2020-3-html])

(The HTML files were provided for convenient web viewing.)

### Hands-on Files

- [Sherlock hands-on files for ML and NN lessons, except the large files][ML-sherlock]
  ([table of contents][ML-sherlock.contents]) -- This also contains
  the Jupyter notebooks above
- [Sherlock large dataset: `sherlock_2apps`][ML-sherlock-large-2apps]
  ([table of contents][ML-sherlock-large-2apps.contents])
- [Sherlock large dataset: `sherlock_18apps`][ML-sherlock-large-18apps]
  ([table of contents][ML-sherlock-large-18apps.contents])

The hands-on files are packed in ZIP format.
The three ZIP files above are mandatory.
To reconstitute: Unzip all the files, preserving the paths, into the same destination directory.


{% include links.md %}

[ml-prez-2022]:              https://docs.google.com/presentation/d/148hT6erWItqMaaMEp_AWuSeDDboTeXcCuygmszuQUxU/edit

[bd-notebook-ws2020-3]:      {{ site.deapsecure_bd_lesson }}/files/ws-2020/BigData-session-3.ipynb
[ml-notebook-ws2020-2]:      {{ site.deapsecure_ml_lesson }}/files/ws-2020/ML-session-2.ipynb
[ml-notebook-ws2020-3]:      {{ site.deapsecure_ml_lesson }}/files/ws-2020/ML-session-3.ipynb
[bd-notebook-ws2020-3-html]: {{ site.deapsecure_bd_lesson }}/files/ws-2020/BigData-session-3.html
[ml-notebook-ws2020-2-html]: {{ site.deapsecure_ml_lesson }}/files/ws-2020/ML-session-2.html
[ml-notebook-ws2020-3-html]: {{ site.deapsecure_ml_lesson }}/files/ws-2020/ML-session-3.html

[ML-sherlock]:               {{ site.deapsecure_ml_lesson }}/files/ML-sherlock.zip
[ML-sherlock-large-2apps]:   {{ site.deapsecure_ml_lesson }}/files/ML-sherlock-large-2apps.zip
[ML-sherlock-large-18apps]:  {{ site.deapsecure_ml_lesson }}/files/ML-sherlock-large-18apps.zip

[ML-sherlock.contents]:      {{ site.deapsecure_ml_lesson }}/files/ML-sherlock.contents
[ML-sherlock-large-2apps.contents]: {{ site.deapsecure_ml_lesson }}/files/ML-sherlock-large-2apps.contents
[ML-sherlock-large-18apps.contents]: {{ site.deapsecure_ml_lesson }}/files/ML-sherlock-large-18apps.contents
