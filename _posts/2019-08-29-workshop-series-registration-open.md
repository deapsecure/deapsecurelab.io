---
title: "2019--2020 Workshop Series: Registration Is Now Open!"
date: 2019-08-29
permalink: /posts/2019/08/workshop-series-registration-open/
tags:
  - workshop
  - registration
  - event
---

{% comment %}
Registration Is Now Open for 2019--2020 Workshop Series!
========================================================
{% endcomment %}

The DeapSECURE workshop series is a training to learn high-performance
computational techniques as applied to cybersecurity.
Come and join us, get your hands dirty while learning to use a
supercomputer to address chalenging cybersecurity research.
We will learn how to perform privacy-preserving computation,
analyze large amounts of data, and use machine learning.

This page contain additional details that you may want to know
before joining this training:

* When are the workshops?
* What will you learn in these workshops?
* What should you expect to gain out of the workshops?
* What are the prerequisite to succeed in this training program?

*(The registration link is at the bottom. Please read on...)*

## Schedule

{% comment %}
The following table shows the planned events and the topics:
{% endcomment %}

**Fall 2019**

|--------------|-------------|-----------------------------|
| Date         | Session     | Topic                       |
|--------------|-------------|-----------------------------|
| 2019-09-13   | Workshop 1  | [Unraveling Cybercrime Using Supercomputer -- **Introduction to HPC**]({% link _lessons/lesson01-hpc.md %})  |
| 2019-09-20   | Hackshop 1  |                             |
|--------------|-------------|-----------------------------|
| 2019-10-04   | Workshop 2  | [HPC-based Secret Computing Using **Homomorphic Encryption**]({% link _lessons/lesson05-crypt.md %})  |
| 2019-10-18   | Hackshop 2  |                             |
|--------------|-------------|-----------------------------|
| 2019-10-25   | Workshop 3  | [Get it Done Fast! **Parallel Programming** with OpenMP and MPI]({% link _lessons/lesson06-par.md %}) |
| 2019-11-08   | Hackshop 3  |                             |
|--------------|-------------|-----------------------------|
| 2019-11-16 (deadline)  | Competition  | A "challenge" competition to really exercise the skills you learned through the preceding three workshops!  |
|--------------|-------------|-----------------------------|

**Spring 2020 (_tentative_)**

|--------------|-------------|-----------------------------|
| Date         | Session     | Topic                       |
|--------------|-------------|-----------------------------|
| 2020-01-24   | Workshop 4  | [Spark: Leveraging **Big Data** for Cybersecurity]({% link _lessons/lesson02-bd.md %})  |
| 2020-01-31   | Hackshop 4  |                             |
|--------------|-------------|-----------------------------|
| 2020-02-07   | Workshop 5  | [**Machine Learning** for Radio Frequency Signal Intelligence]({% link _lessons/lesson03-ml.md %})  |
| 2020-02-14   | Hackshop 5  |                             |
|--------------|-------------|-----------------------------|
| 2020-02-21   | Workshop 6  | [**Deep Learning** for Security -- Wireless Attacks in the Making!]({% link _lessons/lesson04-nn.md %})  |
| 2020-02-28   | Hackshop 6  |                             |
|--------------|-------------|-----------------------------|
| 2020-03-06 (deadline)  | Competition  | A "challenge" competition to really exercise the skills you learned through the preceding three workshops  |
|--------------|-------------|-----------------------------|

Each workshop and hackshop will take place on Friday afternoon,
from 1pm--4pm.


## Format

The DeapSECURE training program consists of three types of
activities: *workshops*, *hackshops*, and *competitions*.

There will be six workshops in the series, covering six modules
on various computing techniques.
Each workshop contains 30-minute research presentation by a
cybersecurity researcher (from ODU),
followed by 2.5 hours of mixed lectures / hands-on activities to
introduce you to one or more computational method(s).
We will introduce you to the basic concepts, issues to be aware of,
and some examples (real computer codes) employing those methods---no
background on these methods is required.
*However, please also see below for prerequisites.*

**New in 2019**: To enhance your learning experience,
we have prepared six additional computing sessions, aptly called "hackshops".
A hackshop is a follow-on session to apply the computational technique
introduced beforehand in the workshop(s) on a certain
cybersecurity-based problem.
During the hackshop, you will be actively working with instructors and TAs
to build your own solution to this problem!

Your participation in the workshops is mandatory.
Your participation is strongly encouraged, because we will be those
ones who are building our own solution to "real-world" problems!


## Prerequisites on Computing Skills

1. **Basic computer programming skill is required**
  to participate in this training.
  You need to have some experience to write
  simple computer programs---about 100 lines of code or less.
  What language you know matters less.

* **Experience with command-line interface** would be highly beneficial,
  although not required to apply.

* The hands-on programs in the workshop use the following
  **computer programming languages**:
  (1) UNIX shell (mainly bash);
  (2) Python;
  (3) C/C++.

  The workshop include short refreshers on UNIX shell and Python.
  For the vast majority of the workshop, only these two languages
  are used.

* In this workshop, we do not assume or require knowledge in
  cybersecurity other than basic general knowledge that most Internet
  and computer users know.



## Rules & Requirements

* This is a hands-on workshop series.
  You are expected to use your laptop to connect to and perform exercises
  on ODU's Turing supercomputer.

* Please bring your own laptop to every workshop and hackshop.

* A certificate of completion will be given to every participant who
  attend five or more workshops.



## What do you expect out of this training?

Expect to get your hands dirty learning supercomputer and how to do
cool stuff with supercomputers!
Do not expect to become an expert on the computational techniques
immediately after taking this workshop.
Nevertheless, you are going to be exposed to the fundamentals of these
techniques through lots of hands-on example programs.
The hands-on exposure will serve as a kick-start for your journey of
the world of high-performance computing and what it can do for
challenges in cybersecurity.
You will be able to pursue more learning on your own after attending
the workshop.

**What this training is NOT about**:
DeapSECURE is not a training on fundamentals of cybersecurity,
cybersecurity operations (penetration test, intrusion detection,
security scanning, etc.), cybersecurity analysis, cyber forensics,
etc.

**To register for DeapSECURE Workshop Series, please visit:**

>   <https://bit.ly/dsecure2019WS>


## Acknowledgements

This training project is funded by the National Science Foundation through OAC
grant #1829771.
