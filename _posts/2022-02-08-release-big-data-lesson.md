---
title: "Open-Source Release of the DeapSECURE 'Big Data' Lesson Module to the Community"
date: 2022-08-02
tags:
  - big data
  - pandas
  - HPC
permalink: /posts/2022/02/release-big-data-lesson/
---

The DeapSECURE team is excited to announce the open source release of
the "Big Data" lesson to the community!

Lesson Website: <https://deapsecure.gitlab.io/deapsecure-lesson02-bd/>

Source: <https://gitlab.com/deapsecure/deapsecure-lesson02-bd/>

The [Big Data lesson]({{site.ds_lesson02_root}}) is now fully released
with the standard Carpentries-style open source licenses (CC-BY for lesson texts & MIT for codes).
Please peruse our lesson materials, teach the lesson,
adapt and reuse our lesson portions as you see fit.
We would love to hear your thoughts, feedback, and issues.

The core hands-on materials, as it is presented in the (pandas-based) lesson,
can be performed using typical computers with at least 8 GB RAM
and about 1 GB disk space.
Learners interested in working with the entire sample "SherLock" dataset
would obviously require significantly more computing resources (both RAM and disk
storage)---this is where HPC and appropriate cloud resources come in.



## Acknowledgements

This training project is funded by the National Science Foundation through the OAC
CyberTraining grant #1829771.