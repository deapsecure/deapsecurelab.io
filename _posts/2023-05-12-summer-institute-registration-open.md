---
title: "2023 DeapSECURE Summer Institute"
date: 2023-05-12
tags:
  - workshop
  - registration
  - event
permalink: /posts/2023/05/2023-summer-institute/
---

The DeapSECURE workshop series is a hands-on training on
*high-performance computational techniques*
emphasizing applications to cybersecurity research.
We are excited to announce the ***Summer 2023 in-person DeapSECURE Workshop***
covering the topics of **High-Performance Computing**, **Big Data**, **Machine Learning** 
and **Neural Networks** for cybersecurity.
We will conduct the workshop in-person. Come and join us, get your hands dirty while 
learning to use a supercomputer to address challenging cybersecurity research!
Techniques taught in DeapSECURE workshops are rather general and transferable 
to other areas including science, engineering, finance, linguistics, etc. 
([Why Cybersecurity?]({% link _pages/workshops.md %}#why-cybersecurity))

[![DeapSECURE brochure]({% link images/Learners-brochure-DeapSECURE-SI-workshop-2023.resized.png %}){: height="400px" width="400px"}](
    {% link files/Learners-brochure-DeapSECURE-SI-workshop-2023.pdf %}
)


This page contain additional details that you may want to know
before joining this training:

* [Who can join these workshops?](#audience)
* [When are the workshops?](#schedule)
* [What is the format of the workshop?](#format)
* [What can you expect to gain out of the workshops?]({% link _pages/workshops.md %}#expect)
* [What are the prerequisites to succeed in this training program?]({% link _pages/workshops.md %}#prereqs)
* [What are rules & requirements of this training?](#rules)

**(The registration link is at the bottom. Please read on...)**



## Workshop Audience
{: #audience}

This workshop is open to students participating in
the Cybersecurity REU program at ODU.
Additional seats are available for ODU HPC users and researchers
who are committed to attend the entire Summer Institute.
This training program is aimed at (1) students with an interest
in cutting-edge cybersecurity research,
(2) those who need to handle and analyze large amounts of data
and/or use machine learning techniques for their research projects.
{% comment %}
We recommend that you _be at least a junior
and have a working knowledge in computer programming_
to apply for this training,
due to the high level of technical and computing contents of the workshop.
{% endcomment %}
**Python programming skill is required.
UNIX shell skill is necessary to interact with HPC systems.**
Please read
[the prerequisites of the training]({% link _pages/workshops.md %}#prereqs)
for more details.



## Schedule
{: #schedule}

**Workshop Dates and Times: Monday-Wednesday, June 5-7, 2023, 9:00am - 4:30pm**

**Sections and Topics:**

|---------------|---------------|--------------------------------------------|
| Date          | Workshop      | Topics                                     |
|---------------|---------------|--------------------------------------------|
| June 05, 2023 |**Workshop 1** (09:00am-12:30pm) | [Introduction to High-Performance Computing][deapsecure-hpc-lesson] |
|---------------|---------------|--------------------------------------------|
| June 05, 2023 |**Workshop 2** (01:00pm-04:30pm) | [Dealing with Big-Data][deapsecure-bd-lesson]                       |
|---------------|---------------|--------------------------------------------|
| June 06, 2023 |**Workshop 3** (09:00am-04:30pm) | [Machine Learning][deapsecure-ml-lesson]                            |
|---------------|---------------|--------------------------------------------|
| June 07, 2023 |**Workshop 4** (09:00am-04:30pm) | [Neural Networks][deapsecure-nn-lesson]                             |
|---------------|---------------|--------------------------------------------|

<!--

 ## Lesson Modules Covered

[Introduction to High-Performance Computing][deapsecure-hpc-lesson]
  1. Analysis of spam collection: A cybersecurity case study for HPC
  2. Working in HPC environment and with job scheduler (SLURM)
  3. Review of UNIX shell commands and shell scripting
  4. Bulk spam processing on HPC

[Dealing with Big Data][deapsecure-bd-lesson]

  1. Tackling cybersecurity challenges with big data analytics
  2. Using Pandas to process and analyze big data
  3. Data cleaning and preparation
  4. Exploratory data analysis, including visualization

[Machine Learning][deapsecure-ml-lesson]

  1. Use cases of machine learning (ML) in cybersecurity
  2. Fundamental concepts in ML
  3. Scikit-learn Python library
  4. Data preprocessing for ML
  5. Building, training, and validating ML models
  6. Feature selection; optimization of ML models

[Deep Learning Using Neural Networks][deapsecure-nn-lesson]

  1. Artificial neurons and neural networks (NN)
  2. Building, training, deploying NN with KERAS
  3. Tuning NN models
  4. Using HPC and graphical processing units (GPUs) to train NN models

-->

There will be multiple short breaks druring each workshop.



## Format
{: #format}

The workshop will be conducted in-person at Old Dominion University 
campus.
It will consist of a mix of lectures / hands-on activities to
introduce learners to basic concepts and practical skills
in HPC, Big Data, Machine Learning and Neural Networks.
Materials will be introduced at an **introductory level**.
Hands-on activities will be carried out in breakout sessions
using **Python** and **Jupyter notebooks**.
There will be teaching assistants to help the learners.



## Rules & Requirements
{: #rules}

1. This is a hands-on training.
   You are expected to bring and use your laptop computer to connect to and perform exercises
   on ODU's Wahab supercomputer.

2. The three-day workshops build upon one another!
   If you sign up and are accepted, you must participation in the entire Summer Institute.
   The number of available seats is very limited, and the demand is high.


### Survey Disclosure

Assessments and participant's feedback are an integral part of this NSF-funded training program;
we require that all participants fill both the pre- and post-workshop surveys.
By participating in this training, you agree to respond to our pre- and post-workshop surveys.
Your individual responses will remain anonymous and confidential.
We will use your collective responses and feedback to help us improve our lessons.
Any published statistics will be reported in ways that preserve the privacy of individual responses.




## Ok, I'm In! How Do I Sign Up for the Workshops?
{: #signup}

<!--  ###COMMENTED###
**To register for DeapSECURE Workshop Series, please visit:**

>   <https://bit.ly/dsecure2020Spring> <br>
>   **Registration deadline: January 21, 2020** <br>
>   ODU MIDAS credentials (email and password) are required to fill in the form.
{: style="background-color:#ffff80;"}

(Full link:
<https://docs.google.com/forms/d/e/1FAIpQLScr-086vbkRrNS5sqMFa0W7fj54Jq_OkNN1-8IBNEBzab-qWg/viewform?usp=sf_link>.
)
 ###END COMMENTED###  -->

**To register for the DeapSECURE Workshop, please:**

>   **[Register Here: https://odu.co1.qualtrics.com/jfe/form/SV_3QKPJ7tRm9s9Tp4](https://odu.co1.qualtrics.com/jfe/form/SV_3QKPJ7tRm9s9Tp4)** <br>
>   **Registration deadline: May 25, 2023** <br>
>
>   (Please allow yourself up to 30 minutes to complete the registration and participant survey.)
{: style="background-color:#ffff80;"}


**IMPORTANT**:
Seating is limited to **TWENTY**.

Due to limited space, we will perform selection on the workshop participants.
You will be notified prior to the workshop start whether you are
accepted into the training program.
By participating, you commit to attend the in-person workshops
for the entire duration of the Institute.



## Acknowledgements

This training project is funded by the National Science Foundation through OAC
CyberTraining grant #1829771.


{% include links.md %}
