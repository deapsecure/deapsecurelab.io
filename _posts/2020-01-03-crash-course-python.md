---
title: "Crash Course on Python"
date: 2020-01-03
permalink: /posts/2020/01/crash-course-python/
tags:
  - courses
  - learning
---

Python programming language will be used for the most parts of
the DeapSECURE training program.
We used to include a brief intro to Python during
[one of the Fall workshops][deapsecure-lesson05-crypt].
However, we strongly encourage you to become familiar with Python
for many good reasons.
It is one of the most popular and easiest programming languages to learn.
It is also used by
[many leading companies, government](
    https://www.fullstackpython.com/companies-using-python.html
), and
[scientific research groups](
    https://wiki.python.org/moin/OrganizationsUsingPython#Science
)
worldwide.
Python programming is one of the most marketable skills today.

Here are several free resources to get started with Python
(for novice and people who never learned computer programming):

1. We recommend learners to learn through the Software Carpentry's lesson:
   ["Plotting and Programming in Python"][lesson-python-swcarpentry].
   This lesson aligns well with DeapSECURE requirements and training format.

   > **For ODU students, faculty and staff**
   > ODU Research Computing team teaches this Python lesson regularly.
   > The latest teaching, about 6 hours total, was recorded and is available
   > at
   > <https://wiki.hpc.odu.edu/ODU/workshop-recordings/2022-fall#plotting-and-programming-with-python>
   >
   > (Note: You must login with ODU MIDAS credentials according to
   > the instructions posted on [this web page](https://wiki.hpc.odu.edu/Howto/Protected-pages)
   > in order to access the recordings.)


2. ["Python for Beginners"][lesson-python-44-videos],
   a free online video course recently developed by Microsoft.
   Courses are short (3-12 minutes each).
   Recommended episodes to watch first (if time is limited):
   1, 2, 3, 5, 9, 13, 25, 27, 29, 38.


If you are already programming regularly with another programming language, then
[Python Tutorial by Geeksfor Geeks][lesson-python-geeks4geeks]
may be more appropriate.

**Want to get more learning resources?**
Jump to the [Beginner's Guide to Python](https://wiki.python.org/moin/BeginnersGuide).


You can install Python on your own computer so that you can practice
in your own time.
We suggest installing [Anaconda Python distribution][anaconda-download]
on your machine
([Windows install guide][anaconda-win-guide],
[MacOS install guide][anaconda-mac-guide]).
When downloading, select Python version >= 3.7.
[Google Colab][google-colab] provides an alternative means
using JupyterLab notebook, which requires no installation on your own machine.


{% include links.md %}
