---
title: "2021 Spring-Summer Workshop Series: Registration Is Now Open!"
date: 2021-03-14
tags:
  - workshop
  - registration
  - event
---

{% comment %}
Registration Is Now Open for 2021 Spring-Summer Online Workshop Series!
=========================================================
{% endcomment %}



The DeapSECURE workshop series is a training on
*high-performance computational techniques*
emphasizing applications to cybersecurity research.
We are excited to announce the DeapSECURE Spring-Summer 2021 online workshop series! 
Due to restrictions imposed in the current situation, we will conduct the workshops online via Zoom. 
In the Spring, we will hold one online workshop: **Introduction to Homomorphic Encryption and Parallel Programming with MPI**. 
In the Summer, we are going to have three consecutive online workshops covering the topics of **Big Data Analytics**, **Machine Learning**, and **Deep Neural Networks**. We have been working tirelessly to create a brand-new online lesson series with an interesting real-world problem as part of our hands-on exercises.
Come and join us, get your hands dirty while learning to use a
supercomputer to address challenging cybersecurity research!
Techniques taught in DeapSECURE workshops are rather general and
transferable to other areas including science, engineering, finance,
linguistics, etc. 



This page contain additional details that you may want to know
before joining this training:

* [Who can join these workshops?](#audience)
* [When are the workshops?](#schedule)
* [What is the format of the workshop?](#format)
* [What will you learn in these workshops?](#schedule)
* [What can you expect to gain out of the workshops?](#expect)
* [What are the prerequisites to succeed in this training program?](#prereqs)

*(The registration link is at the bottom. Please read on...)*



## Workshop Audience
{: #audience}

This training program is aimed at (1) students with an interest
in cutting-edge cybersecurity research,
(2) those who want to learn how to handle and analyze large amounts of data
and/or use machine learning techniques for their research projects.
This workshop series is open to any undergraduate/graduate student,
faculty and staff
at ODU (from any area of study).
However,
we recommend that you _be at least a junior
and have a working knowledge in computer programming_
to apply for this training,
due to the high level of technical and computing contents of the workshop.
Please read [the prerequisites of the training](#prereqs) below.


## Schedule

The following table shows the planned events and the topics:

**Spring-Summer 2021**

|--------------|-------------|-----------------------------|
| Date         | Session     | Topic                       |
|--------------|-------------|-----------------------------|
| 2021-03-19   | Workshop 3  | [Introduction to Homomorphic Encryption and Parallel Programming with MPI]({% link _lessons/lesson06-par.md %})  |
|--------------|-------------|-----------------------------|
| 2021-06-15   | Workshop 4  | [Big Data Analytics]({% link _lessons/lesson02-bd.md %})  |
|--------------|-------------|-----------------------------|
| 2021-06-16   | Workshop 5  | [Machine Learning]({% link _lessons/lesson03-ml.md %})  |
|--------------|-------------|-----------------------------|
| 2021-06-17   | Workshop 6  | [Deep Learning with Neural Networks]({% link _lessons/lesson04-nn.md %})  |
|--------------|-------------|-----------------------------|
| TBD          | Competition | A "challenge" competition to really exercise the skills you learned through the preceding all workshops! |
|--------------|-------------|-----------------------------|

Each workshop and hackshop will take place in the afternoon,
from 1pm--4:30pm.
We will be meeting **online** (via zoom) this semester.


## Format

The DeapSECURE training program consists of two types of
activities: *workshops* and *competitions*. 
We conducted the DeapSECURE workshops in-person in the past semesters, 
but because of the current pandemic situation, 
we are converting this workshop to a 100% online format. 
This will be our first workshop conducted online, 
where you can access the workshop safely via Zoom and TAs will provide real-time technical support.

### Workshop

Each workshop contains 3.5 hours of mixed lectures / hands-on activities to
introduce you to one or more computational method(s).
We will introduce you to the basic concepts
and examples (real computer codes) employing those methods---no
background required on these methods.
*However, please also [see below for prerequisites](#prereqs).*


### Competition

At the end of the workshop series,
we will open a competition for all the DeapSECURE training participants.
The winner will receive a prize!



## What Can I Expect out of This Training?
{: #expect}

Expect to get your hands dirty learning supercomputer and how to do
cool stuff with supercomputers!
You are going to be exposed to computational techniques
through lots of hands-on examples and activities.
True, you do not immediately become an expert on HPC and machine learning
after taking this workshop series,
but you will have started your journey to use these techniques
for your own goals---whether a cybersecurity project,
or science research that require computation.
You will be able to pursue more learning on your own after attending
the workshops.
In the near future we will set up an online learning community
for all the present and past participants of the training program,
so you can continue learning together.

**What this training is NOT about**:
DeapSECURE is not a training on fundamentals of cybersecurity,
cybersecurity operations (penetration test, intrusion detection,
security scanning, etc.), cybersecurity analysis, cyber forensics,
etc.



## Prerequisites on Computing Skills
{: #prereqs}

1. **Basic skill of _writing_ computer programs is required**
  to participate in this training.
  You need to have some experience of building
  simple computer programs---about 100 lines of code or less---preferably
  from scratch.
  The specific language matters less; it is the programming skill that matters.
  Popular languages such as C, C++, Fortran, Python, Matlab, Ruby, would be fine.

2. **Experience with command-line interface** would be highly beneficial,
  although not required to apply.
  Skills on UNIX shell (e.g. bash) can be very helpful.

<!--3. The hands-on activities in the workshop primarily use the
  **Python programming language**.
  Please [see our pointers][crash-python] to several crash courses
  on Python.-->

3. In this workshop, we do not assume or require knowledge in
  cybersecurity other than basic general knowledge that most Internet
  and computer users typically know.

[crash-python]: {% link _posts/2020-01-03-crash-course-python.md %}



## Rules & Requirements
{: #rules}

1. This is a hands-on training.
  You are expected to use your laptop/computer to connect to and perform exercises
  on ODU's Turing supercomputer.

2. Your participation in the workshops is mandatory.
  Your participation is strongly encouraged, because we will be
  challenged to build our own solution to "real-world" problems!

3. A certificate of completion will be given to every participant who
  attend five or more workshops.



## Ok, I'm In! How Can I Sign Up for the Workshops?

To register for DeapSECURE Workshop Series, please
[jump to the sign-up section](#signup).



## Why Cybersecurity?

DeapSECURE is targeted to teach computational skills to people with
interest in cybersecurity, but it is also useful for those who want
to learn about HPC, big data and machine learning in a general way.
*Why the emphasis on cybersecurity, you may ask?*
Two reasons:

1. There is a great need to infuse knowledge about and experience in
   advanced computational techniques into
   the new educational field of "cybersecurity".
   Introducing these techniques in the context familiar to cybersecurity
   students is a very effective way to let them connect them
   to their very subject of concern.

2. As the world is now connected by means of computer technologies,
   cybersecurity has become everyone's business.
   Even though you may not major in cybersecurity (or anything close to it),
   you still own technology devices which must be kept secure.
   Hacking, phishing, spam, malware is an unfortunate fact in the Internet.
   While DeapSECURE is not an education program on cybersecurity per se,
   it will expose you to general awareness of current issues
   in cybersecurity research.
   DeapSECURE workshops feature research presentations by
   ODU's world-leading cybersecurity researchers.



<!--  ###COMMENTED###
## Crash Course on Python
{: #crash-python}

Python programming language will be used from this workshop onward.
We include a brief intro to Python during one of the Fall workshops.
However, we strongly encourage you to become familiar with Python
for many good reasons.
It is one of the most popular and easiest programming languages to learn.
It is also used by
[many leading companies, government](
    https://www.fullstackpython.com/companies-using-python.html
), and
[scientific research groups](
    https://wiki.python.org/moin/OrganizationsUsingPython#Science
)
worldwide.
Python programming is one of the most marketable skills today.

Here are two useful resources for learning Python:

1. ["Python for Beginners"][lesson-python-44-videos],
   a free online video course recently developed by Microsoft.
   Courses are short (3-12 minutes each).
   Recommended episodes to watch first (if time is limited):
   1, 2, 3, 5, 9, 13, 25, 27, 29, 38.
   
2. If you prefer written lessons (no videos),
   please take the Software Carpentry lesson:
   ["Plotting and Programming in Python"][lesson-python-swcarpentry]

You can install Python on your own computer so that you can practice
in your own time.

We suggest installing [Anaconda Python distribution][anaconda-download]
on your machine
([Windows install guide][anaconda-win-guide],
[MacOS install guide][anaconda-mac-guide]).
When downloading, select Python version >= 3.7.
[Google Colab][google-colab] provides an alternative means
using JupyterLab notebook, which requires no installation on your own machine.


[lesson-python-44-videos]: https://channel9.msdn.com/Series/Intro-to-Python-Development?WT.mc_id=python-c9-niner
[lesson-python-swcarpentry]: http://swcarpentry.github.io/python-novice-gapminder/
[anaconda-download]: https://www.anaconda.com/distribution/#download-section
[anaconda-win-guide]: https://problemsolvingwithpython.com/01-Orientation/01.03-Installing-Anaconda-on-Windows/
[anaconda-mac-guide]: https://problemsolvingwithpython.com/01-Orientation/01.04-Installing-Anaconda-on-MacOS/
[google-colab]: https://colab.research.google.com/

 ###END COMMENTED###  -->



## How Do I Sign Up for the Workshops?
{: #signup}

<!--  ###COMMENTED###
**To register for DeapSECURE Workshop Series, please visit:**

>   <https://bit.ly/dsecure2020Spring> <br>
>   **Registration deadline: January 21, 2020** <br>
>   ODU MIDAS credentials (email and password) are required to fill in the form.
{: style="background-color:#ffff80;"}

(Full link:
<https://docs.google.com/forms/d/e/1FAIpQLScr-086vbkRrNS5sqMFa0W7fj54Jq_OkNN1-8IBNEBzab-qWg/viewform?usp=sf_link>.
)
 ###END COMMENTED###  -->

**To register for DeapSECURE Workshop Series, please:**

>   **[Register  Here !!](https://docs.google.com/forms/d/e/1FAIpQLScF7oPZRBUIeyn--f8QtZEJ25R-74Pt1MNDYZzM8M1V2dsalQ/viewform?usp=sf_link)** <br>
>   **Registration deadline: March 14, 2021** <br>
>   ODU MIDAS credentials (email and password) are required to fill in the form.
{: style="background-color:#ffff80;"}



**IMPORTANT**:
Seating is limited to **TWENTY**.

Due to limited space, we perform selection on the workshop participants.
You will be notified prior to the workshop start whether you are
accepted into the training program.
By participating, you commit to attend the two online workshops.



## Acknowledgements

This training project is funded by the National Science Foundation through OAC
grant #1829771.
