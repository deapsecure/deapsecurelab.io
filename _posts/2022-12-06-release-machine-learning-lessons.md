---
title: "Open-Source Release of the DeapSECURE 'Machine Learning' and 'Deep Learning' Lesson Modules to the Community"
date: 2022-12-06
tags:
  - machine learning
  - deep learning
  - neural networks
  - scikit-learn
  - tensorflow
  - keras
  - HPC
permalink: /posts/2022/12/release-machine-learning-lessons/
---

The DeapSECURE team is excited to announce the open source release of
the "Machine Learning" and "Deep Learning" lesson to the community!

**Machine Learning**:

  * Lesson Website: <https://deapsecure.gitlab.io/deapsecure-lesson03-ml/>

  * Source: <https://gitlab.com/deapsecure/deapsecure-lesson03-ml/>

**Deep Learning Using Neural Networks**:

  * Lesson Website: <https://deapsecure.gitlab.io/deapsecure-lesson04-nn/>

  * Source: <https://gitlab.com/deapsecure/deapsecure-lesson04-nn/>

These lessons are now fully released
with the standard Carpentries-style open source licenses (CC-BY for lesson texts & MIT for codes).
Please peruse our lesson materials, teach the lesson,
adapt and reuse our lesson portions as you see fit.
We would love to hear your thoughts, feedback, and issues.
Please use the Gitlab issues to share these with us.

The core hands-on materials, as it is presented in the (pandas-based) lesson,
can be performed using typical computers with at least 8 GB RAM
and about 1 GB disk space.
Learners interested in working with the entire sample "SherLock" dataset
and/or more complex machine learning model architectures
would obviously require significantly more computing resources
(processors, RAM, storage)---this is where they would want to turn to using
HPC and appropriate cloud resources.


> **PARDON OUR CONSTRUCTION** --
> These lessons are still being polished and completed.
> Due to the need for rapid development & conversion to Jupyter-based learning
> during the 2020-2021 development period,
> the various forms of the materials got out-of-sync:
> the web-based lessons, the Jupyter notebooks, and the presentation slides.
> We are in the process of updating our web-based lessons to incorporate
> all the recent changes.
>
> *Interested in helping?*
> The issue pages of the respective lessons have a list of outstanding tasks
> that need to be completed:
>
> * [ML lesson issues](https://gitlab.com/deapsecure/deapsecure-lesson03-ml/-/issues)
> * [NN lesson issues](https://gitlab.com/deapsecure/deapsecure-lesson04-nn/-/issues)
>
> We would welcome contribution to complete these tasks.
> Feedback, problem reports, and ideas can also be posted here.
{: style="background-color:#ffff80;"}


## Acknowledgements

This training project is funded by the National Science Foundation through the OAC
CyberTraining grant #1829771.
