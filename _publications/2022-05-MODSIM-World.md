---
title: "DeapSECURE Computational Training for Cybersecurity: Third-Year Improvements and Impacts"
collection: publication
permalink: /publication/2022-05-MODSIM-World
excerpt: "Detailed account of the DeapSECURE's retooling and full conversion for fully online delivery in 2020-2021; assessment of online workshops in comparison to in-person delivery."
date: 2022-05-11
venue: "MODSIM World 2022"
paperurl: "https://www.modsimworld.org/papers/2022/MSVSCC_2022_InfrastructureSecurityMilitary.pdf"
doi: "https://doi.org/10.25776/7691-p669"
citation: "B. Dodge, J. Strother, R. Asiamah, K. Arcaute, W. Purwanto, M. Sosonkina, and H. Wu (2022). &quot;DeapSECURE Computational Training for Cybersecurity: Third Year Improvements and Impacts&quot; Presented at the MODSIM World 2022 Conference, Norfolk, Virginia, 2022-05-11."
---
**Abstract:**
The Data-Enabled Advanced Training Program for Cybersecurity Research and
Education (DeapSECURE) was introduced in 2018 as a non-degree training
consisting of six modules covering a broad range of cyberinfrastructure
techniques, including high performance computing, big data,
machine learning and advanced cryptography, aimed at reducing the gap
between current cybersecurity curricula and requirements needed for advanced
research and industrial projects.
By its third year, DeapSECURE, like many other educational
endeavors, experienced abrupt changes brought by the COVID-19 pandemic.
The training had to be retooled to adapt to fully online delivery.
Hands-on activities were reformatted to accommodate self-paced learning.
In this paper, we describe and assess the third and fourth years of the project
and compare them with the first half of the project,
which implemented in-person instruction.
We also indicate major improvements and present future opportunities
for this training program to advance the cybersecurity field.

> This paper was initially presented at the
> [15th annual Modeling, Simulation & Visualization Student Capstone Conference 2022][MSVSCC-2022-conf]
> at the Virginia Modeling, Analysis and Simulation Center (VMASC),
> Suffolk, Virginia, April 14, 2022.
> It won the best student paper award for the Military and Security track,
> and was presented again at the [MODSIM World 2022 conference][MODSIM-World-2022-conf]
> in Norfolk, Virginia, May 11, 2022.

[Conference paper](https://www.modsimworld.org/papers/2022/MSVSCC_2022_InfrastructureSecurityMilitary.pdf)

[Presentation slides](https://drive.google.com/file/d/1l48v4IHqa9brHUVj89NBUt1231m4IIsC/view)

[MSVSCC-2022-conf]: https://digitalcommons.odu.edu/msvcapstone/2022/

[MODSIM-World-2022-conf]: https://www.modsimworld.org/about/publications/2022

<!--
Recommended citation: Your Name, You. (2009). "Paper Title Number 1." <i>Journal 1</i>. 1(1).
-->


