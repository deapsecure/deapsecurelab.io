---
permalink: /workshops
title: "DeapSECURE Workshops and Institutes"
excerpt: "About the DeapSECURE Workshops and Institutes"
author_profile: true
---

DeapSECURE provides hands-on training on
*high-performance computational techniques*
emphasizing applications to cybersecurity research.
Since its inception in 2018,
the DeapSECURE team has regularly offered workshops series and summer institutes
to students, faculty and staff at Old Dominion University as well as
select learners across the Commonwealth of Virginia.
We offer these workshops and institutes to refine and teach
the [DeapSECURE lesson modules]({% link _pages/lessons.html %}).
A *workshop* may cover only one of the lesson module,
whereas an *institute* typically covers multiple modules
and take place over consecutive days,
thus providing a concentrated period of time for learning new topics and techniques.
Qualified learners are invited to participate in our upcoming workshops,
watch past workshop's recordings (found on [individual lesson pages]({% link _pages/lessons.html %})),
or simply use our lessons as self-paced learning resources.


> ## [Registration for 2023 Summer Institute Is Open!]({% link _posts/2023-05-12-summer-institute-registration-open.md %})
>
> **June 5-7, 2023, on ODU Norfolk Campus**<br/>
> Please follow the link above to learn more about this workshop
> and to register.
> Please sign up soon. Space is limited!
>
> **Registration deadline: May 25, 2023.**
{: style="background-color:#ffff80;"}

This page contain general information that you may want to know
before joining DeapSECURE workhops:

* [Who can join these workshops?](#audience)
* [What is the format of the workshop?](#format)
* [What can I expect to gain out of the workshops?](#expect)
* [What are the prerequisites to succeed in this training program?](#prereqs)
* [Why Cybersecurity?](why-cybersecurity)

*A specific workshop/institute event may impose additional qualifications
for the learners, offer different mix of lessons,
add and/or customize the training materials.
Please refer to your workshop announcement to learn more.*



## Target Audience
{: #audience}

DeapSECURE workshops are aimed at
(1) students with an interest in cutting-edge cybersecurity research,
and
(2) those who need to use HPC to analyze large amounts of data
and/or employ machine learning techniques for their research projects.
Generally speaking,
due to the high level of technical and computing contents of the workshop,
We recommend that you _be at least a junior
and have working knowledge in computer programming_
to apply for this training.
Please also read more on [the technical prerequisites of the training](#prereqs) below.



## Workshop Format
{: #format}

Generally speaking, the DeapSECURE workshops and institutes are conducted synchronously,
whether in-person or virtually via an appropriate videoconferencing technology.
Each workshop consists of a mix of lectures / hands-on activities to
introduce learners to basic concepts and practical skills
in HPC, Big Data, Machine Learning, and so on.
Materials will be introduced at an **introductory level**.
Hands-on activities are carried out in breakout sessions
using **Python** and **Jupyter notebooks**.
There will be teaching assistants to help the learners.



## What Can I Expect Out of This Training?
{: #expect}

Expect to have fun learning new topics and techniques,
get your hands dirty with cool activities on supercomputers!
While this workshop will not immediately make you an 
expert on big data, machine learning, or such subjects,
you will have started your journey 
to use these techniques for your own goals---whether a cybersecurity project,
or science research that require the use of
these advanced computational techniques.
You will be able to learn more on your own after attending
the workshop.

<!-- FIXME Do this!
In the near future we will set up an online learning 
community for all the present and past participants of the training program,
so you can continue learning together.
-->

**What this training is NOT about**:
DeapSECURE is not a training on fundamentals of cybersecurity,
cybersecurity operations (penetration test, intrusion detection,
security scanning, etc.), cybersecurity threat analysis, cyber forensics,
etc.

<!-- FIXME: Should we refer to SEEDS?

https://seedsecuritylabs.org/
-->



## Prerequisites on Computing Skills
{: #prereqs}

1. **Basic skill of _writing_ computer programs is required**
  to participate in this training.
  You need to have some experience of building
  simple computer programs---about 100 lines of code or less---preferably
  from scratch.
  The specific language matters less; it is the programming skill that matters.
  Popular languages such as C, C++, Fortran, Python, Matlab, Ruby, would be fine.

2. The hands-on activities in the workshop primarily use the
  **Python programming language**, **pandas**, and **scikit-learn**.

3. **Experience with command-line interface** is essential.
  Part of our workshop assumes skills on UNIX shell (e.g. bash/zsh/ksh).

4. In this workshop, we do not assume or require knowledge in
  cybersecurity other than basic general knowledge that most Internet
  and computer users typically know.


<!--
## Crash Courses on UNIX Shell and Python
{: #crash-python}
-->

Because DeapSECURE relies on learner's existing skills on UNIX and Python,
those who are not skilled in UNIX and/or Python
are recommended to take the following lessons offered by Software Carpentry
and taught by ODU's Research Computing Services (RCS)
*prior to joining DeapSECURE workshops* :


### UNIX Shell Lesson (Software Carpentry)
{: #crash-unix}

* The ["UNIX Shell"][lesson-unix-swcarpentry] lesson provides
  hands-on training on the primer of UNIX shell.
* Recording from recent teaching of the UNIX Shell lesson
  (about 3 hours total):
  <https://wiki.hpc.odu.edu/ODU/workshop-recordings/2022-fall#unix-shell>


### Python Lesson (Software Carpentry)
{: #crash-python}

* ["Plotting and Programming in Python"][lesson-python-swcarpentry]
  covers basic Python as well as Pandas.
  Both are used in DeapSECURE.
* Recording from recent teaching of the Python lesson
  (about 6 hours total):
  <https://wiki.hpc.odu.edu/ODU/workshop-recordings/2022-fall#plotting-and-programming-with-python>
* More Python resources are available on our [crash course page][crash-python].

> **ODU credentials are required to access recording!**
>
> You must login with ODU MIDAS credentials according to
> the instructions posted on [this web page](https://wiki.hpc.odu.edu/Howto/Protected-pages)
> in order to access the recordings.

<!--
> ### Learning Resources on Pandas Now Available!
> {: id="resources-pandas"}
>
> Due to the limited amount of time for the workshop,
> we will dive straight into data wrangling with pandas in our workshop.
> While not required, we **strongly recommend** that you do the hands-on learning
> on pandas using the Jupyter notebooks posted on our already
> [**Open-sourced Workshop Material**]({% link _lessons/lesson02-bd.md %}#workshop-resources-ws2020).
> Please go through "Session 1" and "Session 2" of the notebooks.
> You can download and install
> [Anaconda Individual Edition](https://www.anaconda.com/products/individual)
> on your laptop or desktop computer and get a head-start learning!
-->


## Why Cybersecurity?
{: #why-cybersecurity}

DeapSECURE is targeted to teach computational skills to people with
interest in cybersecurity, but it is also useful for those who want
to learn about HPC, big data and machine learning in a general way.
Techniques taught in DeapSECURE workshops are transferable 
to other disciplines, such as: science, engineering, finance, linguistics, etc. 
*Why the emphasis on cybersecurity, you may ask?*
Two reasons:

1. There is a great need to infuse knowledge about and experience in
   advanced computational techniques into
   the new educational field of "cybersecurity".
   Introducing these techniques in the context familiar to cybersecurity
   students is a very effective way to let them connect them
   to their very subject of concern.

2. As the world is now connected by means of computer technologies,
   cybersecurity has become everyone's business.
   Even though you may not major in cybersecurity (or anything close to it),
   you still own technology devices which must be kept secure.
   Hacking, phishing, spam, malware is an unfortunate fact in the Internet.
   While DeapSECURE is not an education program on cybersecurity per se,
   it will expose you to general awareness of current issues
   in cybersecurity research.
   <!--
   DeapSECURE workshops feature research presentations by
   ODU's world-leading cybersecurity researchers.
   -->



{% include links.md %}
