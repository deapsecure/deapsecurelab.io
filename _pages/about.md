---
permalink: /
title: "DeapSECURE -- Data Enabled Advanced Training Platform for Cybersecurity Research and Education"
excerpt: "About the DeapSECURE training project"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---

DeapSECURE is a training program to infuse high-performance
computational techniques into cybersecurity research and education.
It is an NSF-funded project of the ODU School of Cybersecurity
along with the Department of Electrical and Computer Engineering
and the Information Technology Services at ODU.
The DeapSECURE team is actively developing six non-degree training
modules to expose cybersecurity students to advanced CI platforms and
techniques rooted in big data, machine learning, neural networks, and
high-performance programming.
Techniques taught in DeapSECURE workshops are rather general and
transferable to other areas including science, engineering, finance,
linguistics, etc.

Please check out the [sign-up page]({% link _pages/sign-up.md %})
to register for an upcoming workshop.

> ## [Registration for 2023 Summer Institute Is Open!]({% link _posts/2023-05-12-summer-institute-registration-open.md %})
>
> **June 5-7, 2023, on ODU Norfolk Campus**<br/>
> Please follow the link above to learn more about this workshop
> and to register.
> Please sign up soon. Space is limited!
>
> **Registration deadline: May 25, 2023.**
{: style="background-color:#ffff80;"}

> ## Open Source Announcement
> (December 6, 2022)
> **[We open-sourced the DeapSECURE's "Machine Learning" and "Deep Learning" lessons]({% link _posts/2022-12-06-release-machine-learning-lessons.md %})!**
> Please visit the linked announcement and check out our released lessons.
> Adopters and contributors are welcome!

## What’s in the Name?

The name DeapSECURE stands for **D**ata-**E**nabled **A**dvanced Computational Training **P**latform for Cyber**secu**rity **R**esearch and **E**ducation. This name embodies the specific characteristics of this training program:

> **Data-enabled**: DeapSECURE leverages real data sets, such as data generated
  by well-respected cybersecurity researchers and practitioners, in the hands-on 
  activities;

> **Advanced Computational Training Platform**: DeapSECURE introduces advanced
  computational methods such as big data analytics and machine learning and platforms 
  such as the high-performance computer (HPC).
  
> **Cybersecurity research and education** are the target domain and community
  of the DeapSECURE training.

{% comment %}
## [New! Registration for 2020 Fall Online Workshop Series Is Open]({% link _posts/2020-10-01-workshop-series-registration-open.md %})

Please follow the link above to learn more about the DeapSECURE workshop series
(including contents, format, and prerequisites).
There you will also find the link to the registration form.
Space is limited.
**Registration deadline: October 1, 2020 11:59pm.**
{% endcomment %}



## DeapSECURE Training Modules

From year 2019 onward, here are the [six lesson modules]({% link _pages/lessons.html %}) in the order
they were delivered:

1. Unraveling Cybercrime Using Supercomputer --- Introduction to High-Performance Computing (HPC) (module 1)

2. Cryptography for Privacy-Preserving Computation (module 5)

3. Parallel and High-Performance Programming with MPI (module 6)

4. Dealing with Big Data for Cybersecurity (module 2)

5. Machine Learning (module 3)

6. Deep Learning Using Neural Network (module 4)


These modules are delivered through
(1) monthly workshops for ODU students, and
(2) summer institutes for students from other universities and
Research Experiences for Undergraduates participants.
Each workshop includes a lecture providing the motivation and context
for a CI technique, which is then examined during a hands-on session.
The DeapSECURE workshops are designed
to enable students to gain hands-on CI experience
in the context of cybersecurity application
and prepare students with advanced CI techniques
to conduct cutting-edge cybersecurity research
and/or succeed in advanced industrial cybersecurity projects.

> *More about our upcoming workshop and how to sign up can be found* [here]({% link _pages/sign-up.md %}).
>
> Our lesson modules are freely available on the [Lessons page]({% link _pages/lessons.html %}).
{: style="background-color:#F2F3F4;"}



## Background of DeapSECURE Training Project

As the volume and sophistication of cyber-attacks grow, cybersecurity
researchers, engineers and practitioners heavily rely on advanced
cyberinfrastructure (CI) techniques such as big data, machine
learning, and parallel programming, as well as advanced CI platforms,
e.g., cloud and high-performance computing (HPC) to assess cyber
risks, identify and mitigate threats, and achieve defense in
depth.
There is a great need to infuse knowledge about and experience in
advanced CI techniques into
the emerging educational field of "cybersecurity",
as these have yet not been widely introduced in
undergraduate and graduate cybersecurity curricula.
This NSF-sponsored project introduces a unique
"Data-Enabled Advanced Training Program for Cyber Security Research
and Education"
(DeapSECURE), aimed to prepare undergraduate and graduate students
with advanced CI techniques and teach them to use CI resources, tools,
and services to succeed in cutting-edge cybersecurity research and
industrial cybersecurity projects.
Introducing these techniques in the context familiar to cybersecurity
students is a very effective way to let them connect them
 to their very subject of concern.


### Cybersecurity Being Everyone's Business

DeapSECURE is targeted to teach computational skills to people with
interest in cybersecurity, but it is also useful for those who want
to learn about HPC, big data and machine learning in a general way.
One may ask, *Why the emphasis on cybersecurity?*
As the world is now connected by means of computer technologies,
cybersecurity has become everyone's business.
Even though you may not major in cybersecurity (or anything close to it),
you still own technology devices which must be kept secure.
Hacking, phishing, spam, malware is an unfortunate fact in the Internet.
While DeapSECURE is not an education program on cybersecurity per se,
it will expose you to general awareness of current issues
in cybersecurity research.
<!--
   DeapSECURE workshops feature research presentations by
   ODU's world-leading cybersecurity researchers.
-->


## Acknowledgments

DeapSECURE is funded by the U.S. National Science Foundation (NSF)
through the [OAC/CyberTraining grant #1829771][nsf-award-1829771].
More detailed acknowledgments are listed in the
[Acknowledgments]({% link _pages/credits.md %}) page.


{% include links.md %}
