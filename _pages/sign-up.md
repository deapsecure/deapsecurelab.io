---
permalink: /sign-up/
title: "Summer Institute 2023 In-Person DeapSECURE Workshop: Registration Is Now Open!"
author_profile: true
---


## [New! Registration for 2023 Summer Institute In-Person Workshop Series Is Open]({% link _posts/2023-05-12-summer-institute-registration-open.md %})

Please follow the link above to learn more about the DeapSECURE workshop series
(including contents, format, and prerequisites).
There you will also find the link to the registration form.
Space is limited.
**Registration deadline: May 25, 2023, 11:59pm.**

[![DeapSECURE brochure]({% link images/Learners-brochure-DeapSECURE-SI-workshop-2023.resized.png %}){: height="400px" width="400px"}](
    {% link files/Learners-brochure-DeapSECURE-SI-workshop-2023.pdf %}
)
