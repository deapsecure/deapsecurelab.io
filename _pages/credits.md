---
permalink: /credits
title: "DeapSECURE -- Acknowledgments"
excerpt: "Acknowledgments for people, communities and institutions who have made contribution to the DeapSECURE project"
author_profile: true
---

## Project Team

**PI:** Dr. Hongyi "Michael" Wu,
The University of Arizona, Deparment of Electrical and Computer Engineering
(formerly at ODU School of Cybersecurity
and Department of Electrical and Computer Engineering)


**Co-PI:** Dr. Masha Sosonkina,
ODU Department of Electrical and Computer Engineering
(formerly at Department Computational Modeling and Simulation Engineering)

**Co-PI:** Dr. Wirawan Purwanto,
ODU Research Computing Services / Information Technology Services

**Project evaluator:** Dr. Karina Arcaute,
ODU Batten School of Engineering

**Teaching Assistants:**
Issakar Doude, Qiao Zhang, Rui Ning, Liuwan Zhu, Yuming He,
Jewel Ossom, Jacob Strother, Rosby Asiamah, Orion Cohen.
These excellent students greatly helped building-out
the lessons and hands-on materials,
as well as leading and facilitating in the workshops.


## Funding

This training is funded by the National Science Foundation through
[OAC/CyberTraining grant #1829771][nsf-award-1829771]


## Web Technologies

DeapSECURE lessons are built on
[The Carpentries style](https://github.com/carpentries/styles)
version 9.5.3.

This website is powered by the
[academicpages template](https://github.com/academicpages/academicpages.github.io)
and hosted on GitLab pages.

[GitLab pages](https://about.gitlab.com/product/pages/) is a free
service in which websites are built and hosted from code and data
stored in a GitHub repository, automatically updating when a new
commit is made to the respository.


{% include links.md %}
